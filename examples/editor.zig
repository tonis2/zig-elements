const std = @import("std");

const zui = @import("zui");

const BuildResult = zui.Definitions.BuildResult;
const Event = zui.Event;
const Node = zui.Node;
const Document = zui.Document;
const CustomElement = zui.CustomElement;
const Attribute = zui.Attribute;

const parseInt = std.fmt.parseInt;
const print = std.debug.print;

pub const Editor = struct {
    text: []const u8,

    pub fn new(text: []const u8) Editor {
        return Editor{
            .text = text,
        };
    }

    pub fn render(comptime doc: *const Document, node: *Node, result: *BuildResult, state: anytype) !void {
        print("state2 {} \n", .{state.name});

        // var node1 = Node.new("rectangle").set_attributes(&[_]Attribute{
        //     Attribute.new("x", "1000"),
        //     Attribute.new("y", "1000"),
        //     Attribute.new("width", "1000"),
        //     Attribute.new("height", "1000"),
        // });

        // var iterator = node1.iter();

        // while (iterator.next()) |element| {
        //     doc.build(element, result, state) catch unreachable;
        // }

        // defer node1.deinit();
    }
};

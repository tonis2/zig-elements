const std = @import("std");
const print = std.debug.print;

const zui = @import("zui");

const BuildResult = zui.Definitions.BuildResult;
const Event = zui.Event;
const Node = zui.Node;
const Document = zui.Document;
const CustomElement = zui.CustomElement;
const Attribute = zui.Attribute;

const Editor = @import("./editor.zig").Editor;

usingnamespace zui.Elements;

var state = .{
    .name = "test",
};

fn nodeEvent(node: *Node) void {
    print("Click {} \n ", .{state.name});
}

var gpa = std.heap.GeneralPurposeAllocator(.{}){};
const allocator = &gpa.allocator;

pub fn main() !void {
    const doc = Document.register(&[_]CustomElement{
        CustomElement.define(Rectangle, "rectangle"),
        CustomElement.define(Label, "label"),
        CustomElement.define(Editor, "editor"),
    });

    var editor = Editor.new("UUS");

    var node = Node.new("rectangle").set_attributes(&[_]Attribute{
        Attribute.new("x", "100"),
        Attribute.new("y", "100"),
        Attribute.new("width", "400"),
        Attribute.new("height", "400"),
    });

    var node2 = Node.new("label").set_attributes(&[_]Attribute{
        Attribute.new("x", "300"),
        Attribute.new("y", "400"),
        Attribute.new("width", "300"),
        Attribute.new("height", "300"),
    });

    node2.append(Node.new("rectangle").set_attributes(&[_]Attribute{
        Attribute.new("x", "300"),
        Attribute.new("y", "400"),
        Attribute.new("width", "300"),
        Attribute.new("height", "300"),
    }));

    node2.append(Node.new("rectangle").set_attributes(&[_]Attribute{
        Attribute.new("x", "300"),
        Attribute.new("y", "400"),
        Attribute.new("width", "300"),
        Attribute.new("height", "300"),
    }));

    node2.on(Event{
        .name = "click",
        .handler = nodeEvent,
    });

    node.append(node2);

    node.append(Node.new("editor").point(@ptrToInt(&editor)));

    var result = BuildResult.new(allocator);

    // try node2.remove();

    var iterator = node.iter();
    print("bits {} \n ", .{ @sizeOf(Editor)});
    while (iterator.next()) |element| {
        for (element.events.items) |event| {
            event.handler(element);
        }
        try doc.build(element, &result, state);
    }

    // for (result.vertices.items) |vert| {
    //     print("vert {} \n", .{vert.position.x});
    // }

    defer {
        result.deinit();
        _ = gpa.deinit();
        node.deinit();
    }
}

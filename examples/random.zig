// const Text = @import("src/elements/text.zig").Text;
// const Image = @import("src/elements/image.zig").Image;
// const Container = @import("src/elements/container.zig").Container;
const std = @import("std");
const print = std.debug.print;

const Element = struct {
    pub fn show() void {
        print("hello", .{});
    }
};

pub fn main() !void {
    const new: type = Element;

    @call(.{}, new.show, .{});
}

const std = @import("std");
const Node = @import("../node.zig").Node;
const Attribute = @import("../node.zig").Attribute;
const MeshData = @import("../definitions.zig").MeshData;
const Vertex = @import("../definitions.zig").Vertex;
const Vec3 = @import("../definitions.zig").Vec3;
const BuildResult = @import("../definitions.zig").BuildResult;
const Document = @import("../document.zig").Document;

const parseInt = std.fmt.parseInt;
const print = std.debug.print;

pub const Label = struct {
    pub fn render(comptime doc: *const Document, node: *Node, result: *BuildResult, state: anytype) !void {
        print("state {} \n", .{state.name});

        var node1 = Node.new("rectangle").set_attributes(&[_]Attribute{
            Attribute.new("x", "1000"),
            Attribute.new("y", "1000"),
            Attribute.new("width", "1000"),
            Attribute.new("height", "1000"),
        });

        var node2 = Node.new("rectangle").set_attributes(&[_]Attribute{
            Attribute.new("x", "1000"),
            Attribute.new("y", "1000"),
            Attribute.new("width", "1000"),
            Attribute.new("height", "1000"),
        });

        node1.append(node2);

        var iterator = node1.iter();

        while (iterator.next()) |element| {
            doc.build(element, result, state) catch unreachable;
        }

        defer node1.deinit();
    }
};

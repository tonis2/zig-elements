const std = @import("std");
const Node = @import("../node.zig").Node;
const MeshData = @import("../definitions.zig").MeshData;
const Vertex = @import("../definitions.zig").Vertex;
const Vec3 = @import("../definitions.zig").Vec3;
const BuildResult = @import("../definitions.zig").BuildResult;
const Document = @import("../document.zig").Document;

const parseInt = std.fmt.parseInt;
const print = std.debug.print;

pub const Rectangle = struct {
    pub fn render(comptime doc: *const Document, node: *Node, result: *BuildResult, state: anytype) !void {
        const width = node.attributes.get("width") orelse "0";
        const height = node.attributes.get("height") orelse "0";
        const x = node.attributes.get("x") orelse "0";
        const y = node.attributes.get("y") orelse "0";

        var measures = .{
            .width = try parseInt(u32, width, 10),
            .height = try parseInt(u32, height, 10),
            .x = try parseInt(u32, x, 10),
            .y = try parseInt(u32, y, 10),
        };

        const data = MeshData{
            .vertices = &[_]Vertex{
                Vertex{ .position = Vec3.new(measures.x, measures.y, 0), .color = [4]u16{ 1.0, 1.0, 1.0, 1.0 } },
                Vertex{ .position = Vec3.new(measures.x + measures.width, measures.y, 0), .color = [4]u16{ 1.0, 1.0, 1.0, 1.0 } },
                Vertex{ .position = Vec3.new(measures.x + measures.width, measures.y + measures.height, 0), .color = [4]u16{ 1.0, 1.0, 1.0, 1.0 } },
                Vertex{ .position = Vec3.new(measures.x, measures.y + measures.height, 0), .color = [4]u16{ 1.0, 1.0, 1.0, 1.0 } },
            },
            .indices = &[_]u16{ 0, 1, 2, 2, 3, 0 },
        };

        result.add(data) catch unreachable;
    }
};

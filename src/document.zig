const std = @import("std");
const Allocator = std.mem.Allocator;
const Definitions = @import("./definitions.zig");
const Node = @import("./node.zig").Node;
const BuildResult = Definitions.BuildResult;
const BuildConfig = Definitions.BuildConfig;
const print = std.debug.print;

pub const CustomElement = struct {
    tag: type,
    name: []const u8,

    pub fn define(comptime T: type, name: []const u8) CustomElement {
        return CustomElement{
            .tag = T,
            .name = name,
        };
    }
};

pub const Document = struct {
    elements: []const CustomElement,

    pub fn register(comptime elements: []const CustomElement) Document {
        return Document{
            .elements = elements,
        };
    }

    pub fn build(comptime self: *const Document, node: *Node, result: *BuildResult, state: anytype) !void {
        inline for (self.elements) |entry, index| {
            if (std.mem.eql(u8, entry.name, node.tag)) {
                if (node.content_ptr != null) {
                     const i: comptime_int = index;
                    var prt = @intToPtr(*self.elements[i].tag, node.content_ptr.?);
                    print("prt {} \n", .{prt.text});
                } else {
                    try entry.tag.render(self, node, result, state);
                }
            }
        }
    }
};

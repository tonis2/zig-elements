const HashMap = std.AutoHashMap;
const std = @import("std");
const print = std.debug.print;
const testing = std.testing;

pub const Attribute = struct {
    key: []const u8,
    value: []const u8,
    pub fn new(key: []const u8, value: []const u8) Attribute {
        return Attribute{
            .key = key,
            .value = value,
        };
    }
};

pub const EventFn = fn (node: *Node) void;
pub const Event = struct {
    name: []const u8,
    handler: EventFn,
};

const Direction = enum {
    Up, Bottom, Right, Left
};

const NodeIterator = struct {
    current: *Node,
    last_move: Direction,

    const Self = @This();

    pub fn init(node: *Node) NodeIterator {
        return NodeIterator{
            .current = node,
            .last_move = Direction.Right,
        };
    }

    pub fn next(self: *Self) ?*Node {

        // Most top node
        if (self.current.first_node == true and self.last_move == Direction.Right) {
            self.current = self.current;
            self.last_move = Direction.Bottom;
            return self.current;
        }

        //Iterator moves down the tree
        if (self.last_move == Direction.Bottom) {
            //If node has children take the first child
            if (self.current.has_children()) {
                self.current = self.current.children.items[0];
                self.last_move = Direction.Bottom;
                return self.current;
            }

            // When current node has no children, try to move right
            if (self.current.next() != null) {
                self.current = self.current.next().?;
                self.last_move = Direction.Right;
                return self.current;
            }

            //When current node has no children,prev,prext move up but dont return iterator, skiping node
            if ((self.current.is_single()) and (self.current.parent != null)) {
                self.current = self.current.parent.?;
                self.last_move = Direction.Up;
            }
        }

        //Iterator moving up
        if (self.last_move == Direction.Up) {

            //Take right node, when possible
            if (self.current.next() != null) {
                self.current = self.current.next().?;
                self.last_move = Direction.Right;
                return self.current;
            }

            if (self.current.parent != null) {
                self.current = self.current.parent.?;
                self.last_move = Direction.Up;
                return self.next();
            }
        }

        //Iterator moving right
        if (self.last_move == Direction.Right) {
            //Move down, take first child node when node has children
            if (self.current.has_children()) {
                self.current = self.current.children.items[0];
                self.last_move = Direction.Bottom;
                return self.current;
            }

            //When has next node, move to it
            if (self.current.next() != null) {
                self.current = self.current.next().?;
                self.last_move = Direction.Right;
                return self.current;
            }

            // When has no right node, the start moving up with iterator
            if (self.current.next() == null) {
                self.current = self.current.parent.?;
                self.last_move = Direction.Up;

                //Think this, more through, looks wierd
                _ = self.next();

                //Top node, break iter
                if (self.current.first_node == true) {
                    self.current.first_node = false;
                    // self.last_move = Direction.Right;
                    return null;
                }

                if (self.current.prev() != null) {
                    return self.current;
                }
            }
        }

        return null;
    }
};

pub const Node = struct {
    tag: []const u8,
    parent: ?*Node = null,
    index: usize = 0,
    attributes: HashMap([]const u8, []const u8) = HashMap([]const u8, []const u8).init(std.heap.page_allocator),
    children: std.ArrayList(*Node) = std.ArrayList(*Node).init(std.heap.page_allocator),
    events: std.ArrayList(Event) = std.ArrayList(Event).init(std.heap.page_allocator),
    first_node: bool = false,
    content_ptr: ?usize = null,

    pub fn set_attribute(self: *Node, key: []const u8, value: []const u8) !void {
        try self.attributes.put(key, value);
    }

    pub fn set_attributes(self: *Node, attributes: []const Attribute) *Node {
        for (attributes) |item| {
            self.attributes.put(item.key, item.value) catch |err| print("Error setting attribute {}", .{err});
        }
        return self;
    }

    pub fn on(self: *Node, event: Event) void {
        self.events.append(event) catch |err| print("Error adding event {}", .{err});
    }

    pub fn new(tag: []const u8) Node {
        return Node{
            .tag = tag,
        };
    }

    pub fn point(self: *Node, ptr: usize) *Node {
        self.content_ptr = ptr;
        return self;
    }

    pub fn is_single(self: *Node) bool {
        if ((self.next() == null) and (self.children.items.len == 0)) {
            return true;
        }
        return false;
    }

    pub fn has_children(self: *Node) bool {
        if (self.children.items.len > 0) return true;
        return false;
    }

    pub fn iter(self: *Node) NodeIterator {
        self.first_node = true;
        return NodeIterator.init(self);
    }

    pub fn prev(self: *Node) ?*Node {
        if (self.parent != null and self.index != 0) {
            return self.parent.?.children.items[self.index - 1];
        }
        return null;
    }

    pub fn next(self: *Node) ?*Node {
        if (self.parent != null and self.index < self.parent.?.children.items.len - 1) {
            return self.parent.?.children.items[self.index + 1];
        }
        return null;
    }

    pub fn append(self: *Node, child: *Node) void {
        const index = self.children.items.len;

        child.parent = self;
        child.index = index;
        self.children.append(child) catch |err| print("Error appending child {}", .{err});
    }

    pub fn deinit(self: *Node) void {
        var iterator = self.iter();
        while (iterator.next()) |entry| {
            if (!entry.first_node) {
                entry.attributes.deinit();
                //Todo: cant't deinit children
                // entry.children.deinit();
                entry.events.deinit();
            }
        }

        self.children.deinit();
        self.attributes.deinit();
        self.events.deinit();
    }

    pub fn search(self: *Node, key: []const u8, value: []const u8) ?*Node {
        var iterator = self.iter();
        while (iterator.next()) |entry| {
            const key_value = entry.attributes.get(key);
            if (key_value != null and std.mem.eql(u8, key_value.?, value)) return entry;
        }
        return null;
    }

    pub fn remove(self: *Node) !void {
        if (self.parent != null) {
            if (self.has_children()) {
                var iterator = self.iter();
                while (iterator.next()) |entry| {
                    if (!entry.first_node) {
                        entry.remove() catch unreachable;
                    }
                }
                // try self.children.replaceRange(0, self.children.items.len, &[_]*Node{});
            }

            self.children.deinit();
            self.attributes.deinit();
            self.events.deinit();

            const newlen = self.parent.?.children.items.len - 1;

            if (newlen == self.index) {
                _ = self.parent.?.children.pop();
                return;
            }

            for (self.parent.?.children.items[self.index..newlen]) |*b, j| {
                self.parent.?.children.items[self.index + 1 + j].index = j;
                b.* = self.parent.?.children.items[self.index + 1 + j];
            }

            self.parent.?.children.items[newlen] = undefined;
            self.parent.?.children.items.len = newlen;
        }
    }
};

test "Children iterator works" {
    var node = Node.new("top");
    var node1 = Node.new("node1");
    var node2 = Node.new("node2");

    node1.append(&node2);
    node1.append(&Node.new("node3"));
    node.append(&node1);
    node2.append(&Node.new("node4"));

    var iterator = node.iter();

    testing.expectEqualSlices(u8, iterator.next().?.tag, "top");
    testing.expectEqualSlices(u8, iterator.next().?.tag, "node1");
    testing.expectEqualSlices(u8, iterator.next().?.tag, "node2");
    testing.expectEqualSlices(u8, iterator.next().?.tag, "node4");
    testing.expectEqualSlices(u8, iterator.next().?.tag, "node3");
}

test "Search node" {
    var node = Node.new("top");
    var node1 = Node.new("node1");
    var node4 = Node.new("node4");
    var node6 = Node.new("node6");
    var node9 = Node.new("node9").set_attributes(&[_]Attribute{Attribute.new("id", "find_me")});

    node.append(&node1);
    node.append(&Node.new("node2"));
    node.append(&Node.new("node3"));

    node1.append(&node4);
    node1.append(&Node.new("node5"));
    node1.append(&node6);
    node1.append(&Node.new("node11"));

    node4.append(&Node.new("node7"));

    node6.append(&Node.new("node8"));
    node6.append(node9);
    node6.append(&Node.new("node12"));

    node9.append(&Node.new("node10"));

    var result = node.search("id", "find_me");
    testing.expectEqualSlices(u8, result.?.tag, "node9");
}

test "Children iterator works2" {
    var node = Node.new("top");
    var node1 = Node.new("node1");
    var node4 = Node.new("node4");
    var node6 = Node.new("node6");
    var node9 = Node.new("node9");

    node.append(&node1);
    node.append(&Node.new("node2"));
    node.append(&Node.new("node3"));

    node1.append(&node4);
    node1.append(&Node.new("node5"));
    node1.append(&node6);
    node1.append(&Node.new("node11"));

    node4.append(&Node.new("node7"));

    node6.append(&Node.new("node8"));
    node6.append(&node9);
    node6.append(&Node.new("node12"));

    node9.append(&Node.new("node10"));

    var iterator = node.iter();

    testing.expectEqualSlices(u8, iterator.next().?.tag, "top");
    testing.expectEqualSlices(u8, iterator.next().?.tag, "node1");
    testing.expectEqualSlices(u8, iterator.next().?.tag, "node4");
    testing.expectEqualSlices(u8, iterator.next().?.tag, "node7");
    testing.expectEqualSlices(u8, iterator.next().?.tag, "node5");
    testing.expectEqualSlices(u8, iterator.next().?.tag, "node6");
    testing.expectEqualSlices(u8, iterator.next().?.tag, "node8");
    testing.expectEqualSlices(u8, iterator.next().?.tag, "node9");
    testing.expectEqualSlices(u8, iterator.next().?.tag, "node10");
    testing.expectEqualSlices(u8, iterator.next().?.tag, "node12");
    testing.expectEqualSlices(u8, iterator.next().?.tag, "node11");
    testing.expectEqualSlices(u8, iterator.next().?.tag, "node2");
    testing.expectEqualSlices(u8, iterator.next().?.tag, "node3");
}

test "test3" {
    var node = Node.new("top");
    var node1 = Node.new("node1");
    var node2 = Node.new("node2");

    node.append(&Node.new("node3"));
    node.append(&Node.new("node4"));
    node.append(&Node.new("node5"));
    node.append(&Node.new("node6"));

    node.append(&node1);
    node.append(&Node.new("node7"));
    node.append(&Node.new("node8"));
    node.append(&Node.new("node9"));
    node.append(&Node.new("node10"));
    node1.append(&node2);

    node2.append(&Node.new("node11"));
    node2.append(&Node.new("node12"));
    node2.append(&Node.new("node13"));
    node2.append(&Node.new("node14"));

    var iterator = node.iter();

    testing.expectEqualSlices(u8, iterator.next().?.tag, "top");
    testing.expectEqualSlices(u8, iterator.next().?.tag, "node3");
    testing.expectEqualSlices(u8, iterator.next().?.tag, "node4");
    testing.expectEqualSlices(u8, iterator.next().?.tag, "node5");
    testing.expectEqualSlices(u8, iterator.next().?.tag, "node6");
    testing.expectEqualSlices(u8, iterator.next().?.tag, "node1");
    testing.expectEqualSlices(u8, iterator.next().?.tag, "node2");
    testing.expectEqualSlices(u8, iterator.next().?.tag, "node11");
    testing.expectEqualSlices(u8, iterator.next().?.tag, "node12");
    testing.expectEqualSlices(u8, iterator.next().?.tag, "node13");
    testing.expectEqualSlices(u8, iterator.next().?.tag, "node14");
    testing.expectEqualSlices(u8, iterator.next().?.tag, "node7");
    testing.expectEqualSlices(u8, iterator.next().?.tag, "node8");
    testing.expectEqualSlices(u8, iterator.next().?.tag, "node9");
    testing.expectEqualSlices(u8, iterator.next().?.tag, "node10");
}
